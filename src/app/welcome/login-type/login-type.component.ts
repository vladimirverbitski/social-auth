import { Component, OnInit } from '@angular/core';
import { MainAuthService } from 'src/app/auth/auth.service';
import { Router } from '@angular/router';
import { AuthService } from "angularx-social-login";
import { FacebookLoginProvider, GoogleLoginProvider } from "angularx-social-login";
import { environment } from 'src/environments/environment';

declare var FB: any;

@Component({
  selector: 'app-login-type',
  templateUrl: './login-type.component.html',
  styleUrls: ['./login-type.component.css']
})
export class LoginTypeComponent implements OnInit {

  constructor(
    private authService: AuthService,
    private mainAuthService:  MainAuthService,
    public router: Router
  ) { }

  ngOnInit() {
    (window as any).fbAsyncInit = function() {
      FB.init({
        appId      : environment.facebookKey,
        cookie     : true,
        xfbml      : true,
        version    : environment.fbApiV
      });
        
      FB.AppEvents.logPageView();   
        
    };

    (function(d, s, id){
      var js, fjs = d.getElementsByTagName(s)[0];
      if (d.getElementById(id)) {return;}
      js = d.createElement(s); js.id = id;
      js.src = "https://connect.facebook.net/en_US/sdk.js";
      fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));
  }

  facebookLogin() {
    FB.login((r) => {
        console.log('submitLogin',r);
        if (r.authResponse) {
          this.mainAuthService.setToken(r.authResponse.accessToken, 'facebook');
          this.router.navigate(['/admin']);
        } else {
          alert('Ops, something went wrong! Try again!')
        }
    });
  }
 
  googleLogin(): void {
    this.authService.signIn(GoogleLoginProvider.PROVIDER_ID).then((r) => {
      if (r.idToken) {
        this.mainAuthService.setToken(r.idToken, 'google');
        this.router.navigate(['/admin']);
      } else {
        alert('Ops, something went wrong! Try again!')
      }
    });
  }

  logOut(): void {
    this.authService.signOut().then( r => {
      this.mainAuthService.destroyToken();
    })
  }

}
