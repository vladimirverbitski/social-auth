import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginTypeComponent } from './login-type/login-type.component';

const routes: Routes = [
  {
    path: '',
    component: LoginTypeComponent
  },
  
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class WelcomeRoutingModule { }
