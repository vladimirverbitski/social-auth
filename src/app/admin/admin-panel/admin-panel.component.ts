import { AuthService } from 'angularx-social-login';
import { Component, OnInit } from '@angular/core';
import { MainAuthService } from 'src/app/auth/auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-admin-panel',
  templateUrl: './admin-panel.component.html',
  styleUrls: ['./admin-panel.component.css']
})
export class AdminPanelComponent implements OnInit {

  constructor(
    private authService: AuthService,
    private mainAuthService: MainAuthService,
    public router: Router
  ) { }

  ngOnInit() {
  }

  logOut(): void {
    this.authService.signOut().then( r => {
      this.mainAuthService.destroyToken();
      this.router.navigate(['/welcome']);
    });
  }

}
