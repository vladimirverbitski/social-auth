import { Injectable } from '@angular/core';
import { Router } from '@angular/router';

const TOKEN = 'accessToken';
const AGENT = 'agent';

@Injectable({
  providedIn: 'root'
})
export class MainAuthService {
  // i was needed to change class name beacause of confilcts with social auth plugin
  constructor(
    public router: Router
  ) { }

  setToken(token: string, service: string): void {
    localStorage.setItem(TOKEN, token);
    localStorage.setItem(AGENT, service);
  }

  get isLogged() {
    return localStorage.getItem(TOKEN) != null;
  }

  get agent() {
    return localStorage.getItem(AGENT);
  }

  destroyToken(): void {
    localStorage.removeItem(TOKEN);
    localStorage.removeItem(AGENT);
  }

  logOut(): void {
    this.destroyToken();
    this.router.navigate(['/welcome']);
  }
}
